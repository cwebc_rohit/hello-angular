import { Hero } from './hero';

export const HEROES : Hero[] = [
{id : 13, name: 'Rohit'},
{id : 14, name: 'Ashok'},
{id : 15, name: 'Bharat'},
{id : 16, name: 'Lalit'},
{id : 17, name: 'Sunil'},
{id : 18, name: 'Sony'},
{id : 19, name: 'Amit'},
{id : 20, name: 'Rajan'},
{id : 21, name: 'Vicky'},
{id : 22, name: 'Ronit'},
{id : 23, name: 'Raghav'}
];